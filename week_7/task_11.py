import sys

words = []
counter = {}
result = []

for line in sys.stdin.readlines():
    words += line.split()

for i in words:
    counter[i] = counter.get(i, 0) + 1

for key, values in counter.items():
    result.append((values, key))

for word in sorted(result, key=lambda x: (-x[0], x[1])):
    print(word[1])
