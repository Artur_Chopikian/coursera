myDict = {}
count = 0

with open('input.txt', 'r', encoding='utf8') as file:
    voices = file.readlines()

for i in voices:
    myDict[i] = myDict.get(i, 0) + 1

with open('output.txt', 'w', encoding='utf8') as file:
    for key, value in sorted(myDict.items(), key=lambda x: x[1], reverse=True):
        if value > len(voices) / 2:
            file.write(key)
            break
        elif count < 2:
            file.write(key)
            count += 1
