N = int(input())

result = []

for _ in range(N):
    one = int(input())
    oneSet = set()
    for i in range(one):
        oneSet.add(input())
    result.append(oneSet)

one_student = set()

for elem in result:
    for i in elem:
        one_student.add(i)

all_student = one_student

for elem in result:
    all_student &= elem

if len(all_student) > 0:
    print(len(all_student))
    for i in sorted(all_student, reverse=True):
        print(i)
else:
    print(0)

if len(one_student) > 0:
    print(len(one_student))
    for i in sorted(one_student, reverse=True):
        print(i)
else:
    print(0)
