all_word = []
counter = {}

with open('input.txt', 'r', encoding='utf-8') as file:
    for line in file.readlines():
        all_word += line.split()

for word in all_word:
    counter[word] = counter.get(word, 0) + 1
    print(counter[word] - 1, end=' ')
