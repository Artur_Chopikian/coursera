n = int(input())

result = set(map(str, range(1, n + 1)))

while True:
    nums = set(input().split())
    if 'HELP' in nums:
        break
    answer = input()
    if answer == 'YES':
        result &= nums
    else:
        result -= nums

print(*sorted(result))
