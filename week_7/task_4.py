import sys

text = sys.stdin.readlines()

result = []

for elem in text:
    for i in elem.split():
        result.append(i)

print(len(set(result)))
