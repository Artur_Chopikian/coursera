n = int(input())
listDict = []

for i in range(n):
    text = input().split()
    listDict.append([text[0], text[1]])

search = input()

for i in range(len(listDict)):
    if search in listDict[i]:
        index = abs(listDict[i].index(search) - 1)
        print(listDict[i][index])
        break
