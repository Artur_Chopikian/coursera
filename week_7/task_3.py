nums = list(map(int, input().split()))

mySet = set()

for elem in nums:
    if elem in mySet:
        print('YES')
    else:
        print('NO')
    mySet.add(elem)
