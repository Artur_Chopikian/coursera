import sys

words = []

for line in sys.stdin.readlines():
    words += line.split()

counter = {}

for i in words:
    counter[i] = counter.get(i, 0) + 1

maximum = max(counter.values())

result = []

for key, value in counter.items():
    if value == maximum:
        result.append(key)

print(sorted(result)[0])
