from math import sqrt

print(2, *filter(lambda x: all(map(lambda n: x % n, range(2, int(sqrt(x)) + 1))), range(3, int(input()) + 1)))
