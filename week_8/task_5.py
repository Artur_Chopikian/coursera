from functools import reduce

print(reduce(lambda a, x: a * x, map(int, input().split())) ** 5)
