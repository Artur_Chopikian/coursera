data = []

with open('input.txt', 'r', encoding='utf8') as file:
    for line in file:
        text = list(map(int, line.split()[-3:]))
        data.append(text)

n = data[0][0]

data_rating = []

for one, two, tree in data[1:]:
    if one >= 40 and two >= 40 and tree >= 40:
        data_rating.append(one + two + tree)

data_rating.sort(reverse=True)

max_rating = max(data_rating)

with open('output.txt', 'w', encoding='utf8') as file:
    if len(data_rating) > n:
        if data_rating.count(max(data_rating)) > n:
            file.write('1')
        else:
            for i in range(len(data_rating)):
                if data_rating[i] == data_rating[n]:
                    file.write(f'{data_rating[i - 1]}')
                    break

    elif len(data_rating) <= n:
        file.write('0')
