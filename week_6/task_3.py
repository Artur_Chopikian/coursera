s_n = list(map(int, input().split()))
data_users = list(int(input()) for i in range(s_n[1]))
data_users.sort()
result = 0

for i in range(len(data_users)):
    result += data_users[i]
    if result <= s_n[0]:
        continue
    else:
        print(i)
        break
else:
    print(len(data_users))
