n = int(input())

data = [input().split() for i in range(n)]


def _sort_(key):
    return int(key[1])


data.sort(key=_sort_, reverse=True)

for name in data:
    print(name[0])
