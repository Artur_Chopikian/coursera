def sort(key):
    return key[1]


n = int(input())
cities = list(map(int, input().split()))
cities = [(i + 1, cities[i]) for i in range(len(cities))]
cities.sort(key=sort)

m = int(input())
shelters = list(map(int, input().split()))
shelters = [(i + 1, shelters[i]) for i in range(len(shelters))]
shelters.sort(key=sort)

result = []

j = 0

for i in range(len(cities)):
    while j < len(shelters):
        if cities[i][1] > shelters[j][1] and j + 1 < len(shelters):
            j += 1
        else:
            if abs(cities[i][1] - shelters[j][1]) < abs(cities[i][1] - shelters[j - 1][1]):  # тут задовгий рядок
                result.append((shelters[j][0], cities[i][0]))
            else:
                result.append((shelters[j - 1][0], cities[i][0]))
            break

result.sort(key=sort)

for i in result:
    print(i[0], end=' ')
