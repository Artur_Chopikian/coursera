data = []


def _sort_(key):
    return key[0]


with open('input.txt', 'r', encoding='utf8') as file:
    for line in file:
        line = line.split()
        line.pop(2)
        data.append(line)

data.sort(key=_sort_)

with open('output.txt', 'w', encoding='utf8') as file:
    for line in data:
        file.writelines(' '.join(line) + '\n')
