marks = list(map(int, input().split()))
countMarks = [0] * (100 + 1)
for mark in marks:
    countMarks[mark] += 1
print(countMarks)
for nowMark in range(100 + 1):
    print((str(nowMark) + ' ') * countMarks[nowMark], end='')
