import math

N = 16
n = int(input())
a = list(map(int, input().split(', ')))


def sin(num):
    return math.sin(math.radians(num)) ** 2


def cos(num):
    return math.cos(math.radians(num)) ** 2


n = bin(n).replace('0b', '')

one = sin(a[0]) if n[0] == '1' else cos(a[0])
two = sin(a[1]) if n[1] == '1' else cos(a[1])
tree = sin(a[2]) if n[2] == '1' else cos(a[2])

result = N * (one * two * tree)

print(f'{result:.03f}')
