n = int(input())

result = 0

while n != 0:
    result += 1 / n ** 2
    n -= 1

print('{:.5f}'.format(result))
