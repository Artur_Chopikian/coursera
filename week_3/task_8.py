a = float(input())
b = float(input())
c = float(input())

if a == 0:
    if b == 0 and c == 0:
        print(3)
    elif b == 0:
        print(0)
    elif c == 0:
        print(1, 0)
    else:
        x = - c / b
        print(1, x)
else:
    d = b ** 2 - 4 * a * c

    if d > 0:
        x1 = (- b + d ** 0.5) / (2 * a)
        x2 = (- b - d ** 0.5) / (2 * a)
        if x1 < x2:
            print(2, '%.6f %.6f' % (x1, x2))
        else:
            print(2, '%.6f %.6f' % (x2, x1))
    elif d == 0:
        x = - b / (2 * a)
        print(1, '%.6f' % x)
    else:
        print(0)
