n = int(input())
# print('The next number for the number %s is %s.' % (n, n + 1))
print('The next number for the number ', n, ' is ', n + 1, sep='', end='.\n')
print('The previous number for the number %s is %s.' % (n, n - 1))
