number = int(input())

text_1 = '    _~_    '
text_2 = '   (o o)   '
text_3 = '  /  V  \  '
text_4 = ' /(  _  )\ '
text_5 = '   ^^ ^^   '


print(text_1*number,
      text_2*number,
      text_3*number,
      text_4*number,
      text_5*number,
      sep='\n')
