x1 = float(input())
y1 = float(input())
x2 = float(input())
y2 = float(input())
x3 = float(input())
y3 = float(input())


def perimeter(x1, y1, x2, y2, x3, y3):
    result = ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5
    result += ((x3 - x2) ** 2 + (y3 - y2) ** 2) ** 0.5
    result += ((x1 - x1) ** 2 + (y1 - y3) ** 2) ** 0.5
    return result


print(perimeter(x1, y1, x2, y2, x3, y3))
