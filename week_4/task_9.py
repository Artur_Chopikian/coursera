n = int(input())
m = int(input())


def ReduceFraction(n, m):
    if n % 5 == 0 and m % 5 == 0:
        return ReduceFraction(n / 5, m / 5)
    elif n % 3 == 0 and m % 3 == 0:
        return ReduceFraction(n / 3, m / 3)
    elif n % 2 == 0 and m % 2 == 0:
        return ReduceFraction(n / 2, m / 2)
    else:
        return int(n), int(m)


print(*ReduceFraction(n, m))
