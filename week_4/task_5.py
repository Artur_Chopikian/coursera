# import time

n = int(input())


# start = time.time()


def IsPrime(n):
    i = 2
    while i <= (n ** 0.5):
        if n % i == 0:
            return 'NO'
        i += 1
    return 'YES'


print(IsPrime(n))
# print(f'{(time.time() - start):.3f}')
