n = int(input())


def MinDivisor(n):
    x = n ** 0.5
    y = 2
    while y <= x:
        if n % y == 0:
            return y
        y += 1
    return n


print(MinDivisor(n))
