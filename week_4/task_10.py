def sum_number(n=1):
    while n != 0:
        n = int(input())
        return sum_number(n) + n
    else:
        return n


print(sum_number())
