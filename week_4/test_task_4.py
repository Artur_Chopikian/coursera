x = float(input())
y = float(input())


def IsPointInArea(x, y):
    one = (x + 1) ** 2 + (y - 1) ** 2 <= 4
    two = y >= -x
    tree = y >= 2 * x + 2
    four = y <= -x
    five = y <= 2 * x + 2
    six = (x + 1) ** 2 + (y - 1) ** 2 >= 4
    return (one and two and tree) or (four and five and six)


if IsPointInArea(x, y):
    print('YES')
else:
    print('NO')
