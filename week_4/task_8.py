a = float(input())
n = int(input())


def power(a, n):
    if n == 0:
        return 1
    elif n % 2 != 0:
        return power(a, n - 1) * a
    elif n % 2 == 0:
        return (a ** 2) ** (n / 2)


print(power(a, n))
