import time

# numbers = list(map(int, input().split()))
names = ['artur', 'shop.name', 'lilia', 'timur', 'app.store', 'shopArtur', 'david']


def check_name(name):
    sort = ['shop', 'store']
    for i in sort:
        if i in name:
            return False
    return name


def timer(function):
    def decorator(*args, **kwargs):
        start = time.time()
        function(*args, **kwargs)
        end = time.time()
        print(f'----------\n{end - start:.10f}\n----------')

    return decorator


def main():
    print('2')


if __name__ == '__main__':
    main()
