n = int(input())

f_1 = '+___ '
f_2 = tuple(f'|{i} / ' for i in range(1, n + 1))
f_3 = '|__\ '
f_4 = '|    '

print(f_1 * n)
print(*f_2, sep='')
print(f_3 * n)
print(f_4 * n)
