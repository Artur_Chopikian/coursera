numbers = list(map(int, input().split()))

more_than_zero = [i for i in numbers if i > 0]

print(min(more_than_zero))
