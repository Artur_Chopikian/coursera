numbers = list(map(int, input().split()))

result = [0, 0]

for i in range(len(numbers)):
    if numbers[i] >= result[0]:
        result[0] = numbers[i]
        result[1] = i

print(*result)
