numbers = list(map(int, input().split()))

first = 0
result = []

for i in range(len(numbers)):
    if i + 1 < len(numbers):
        if numbers[i + 1] > numbers[i]:
            result.append(numbers[i + 1])

print(*result)
