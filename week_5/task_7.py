numbers = list(map(int, input().split()))

result = [i for i in numbers if i > 0]

print(len(result))
