numbers = list(map(int, input().split()))

for i in range(0, len(numbers), 2):
    if i == len(numbers) - 1:
        break
    x = numbers[i]
    numbers[i] = numbers[i + 1]
    numbers[i + 1] = x

print(*numbers)
