a = int(input())
b = int(input())

result = tuple()

if b > a:
    result = tuple(range(a, b + 1))
    print(*result)
elif a > b:
    result = tuple(range(b, a + 1))
    print(*result[::-1])
else:
    print(a)
