n = int(input())
numbers = list(map(int, input().split()))
x = int(input())

difference = 10000
result = 0

for i in numbers:
    if i == x:
        result = i
        break

    if abs(x - i) < abs(difference):
        difference = x - i
        result = i

print(result)
