numbers = list(map(int, input().split()))

minimum = min(numbers)
maximum = max(numbers)

index_min = numbers.index(minimum)
index_max = numbers.index(maximum)

numbers[index_min] = maximum
numbers[index_max] = minimum

print(*numbers)
