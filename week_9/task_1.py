from sys import stdin


class Matrix(object):
    def __init__(self, matrix):
        self.matrix = list(map(lambda item: item[0:], matrix))

    def __str__(self):
        string = ''
        for i, matrix in enumerate(self.matrix):
            if i + 1 == len(self.matrix):
                string += '\t'.join(map(str, matrix))
            else:
                string += '\t'.join(map(str, matrix)) + '\n'
        return string

    def size(self):
        return len(self.matrix), len(self.matrix[0])


exec(stdin.read())
