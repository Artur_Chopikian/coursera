from sys import stdin


class MatrixError(BaseException):
    def __init__(self, a, b):
        self.matrix1 = Matrix(a.matrix)
        self.matrix2 = Matrix(b.matrix)

    def __str__(self):
        return '{} {}'.format(self.matrix1, self.matrix2)


class Matrix(object):
    def __init__(self, matrix):
        self.matrix = list(map(lambda item: item[0:], matrix))

    def __str__(self):
        string = ''
        for i, matrix in enumerate(self.matrix):
            if i + 1 == len(self.matrix):
                string += '\t'.join(map(str, matrix))
            else:
                string += '\t'.join(map(str, matrix)) + '\n'
        return string

    def __add__(self, other):
        result = []
        if len(self.matrix) == len(other.matrix) and \
                len(self.matrix[0]) == len(other.matrix[0]):
            for i in range(len(self.matrix)):
                result.append(list(
                    map(lambda x, y: x + y, self.matrix[i], other.matrix[i]))
                )
            return Matrix(result)
        else:
            raise MatrixError(self, other)

    def __mul__(self, other):
        result = []
        if isinstance(other, int) or isinstance(other, float):
            for i in range(len(self.matrix)):
                result.append(list(map(lambda x: x * other, self.matrix[i])))
            return Matrix(result)
        elif isinstance(other, Matrix):
            if len(self.matrix) == len(other.matrix):
                for i in range(len(self.matrix)):
                    result.append(list(map(lambda x, y: x * y, self.matrix[i], other.matrix[i])))
                return Matrix(result)
            else:
                error = MatrixError(self, other)
                raise error

    __rmul__ = __mul__

    def transpose(self):
        result = list(map(list, zip(*self.matrix)))
        self.matrix = result
        return Matrix(result)

    def size(self):
        return len(self.matrix), len(self.matrix[0])

    @staticmethod
    def transposed(other):
        result = list(map(list, zip(*other.matrix)))
        return Matrix(result)


# exec(stdin.read())
