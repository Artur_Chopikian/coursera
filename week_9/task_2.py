from sys import stdin


class Matrix(object):
    def __init__(self, matrix):
        self.matrix = list(map(lambda item: item[0:], matrix))

    def __str__(self):
        string = ''
        for i, matrix in enumerate(self.matrix):
            if i + 1 == len(self.matrix):
                string += '\t'.join(map(str, matrix))
            else:
                string += '\t'.join(map(str, matrix)) + '\n'
        return string

    def __add__(self, other):
        result = []
        for i in range(len(self.matrix)):
            result.append(list(
                map(lambda x, y: x + y, self.matrix[i], other.matrix[i]))
            )
        return Matrix(result)

    def __mul__(self, other):
        result = []
        for i in range(len(self.matrix)):
            result.append(list(map(lambda x: x * other, self.matrix[i])))
        return Matrix(result)

    def __rmul__(self, other):
        return self.__mul__(other)

    def size(self):
        return len(self.matrix), len(self.matrix[0])


exec(stdin.read())
