a = int(input())
b = int(input())
c = int(input())
d = int(input())

if c == 0 and d == 0:
    print('NO')
elif c != 0 or d != 0:
    if a == 0 and b == 0:
        print('INF')
    elif a == 0 and b != 0:
        print('NO')
    elif b == 0 and a != 0:
        print(0)
    elif a != 0 and b != 0:
        x = int(- b / a)
        if c * x + d != 0 and (a * x + b) / (c * x + d) == 0:
            print(x)
        else:
            print('NO')
else:
    if (a == c and b == d) or (a == 0 and b == 0):
        print('INF')
    elif a == 0 and b != 0:
        print('NO')
    elif b == 0 and a != 0:
        print(0)
    elif a != 0 and b != 0:
        x = int(- b / a)
        if c * x + d != 0 and (a * x + b) / (c * x + d) == 0:
            print(x)
        else:
            print('NO')
