result = 0
maximum = 0
while True:
    n = int(input())
    if n != 0:
        if n >= maximum:
            result = maximum
            maximum = n
        if result < n < maximum:
            result = n
    else:
        break
print(result)
