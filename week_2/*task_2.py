k = int(input())
m = int(input())
n = int(input())
if n <= k:
    result = 2 * m
elif 0 < n % k <= int(k / 2):
    result = 2 * m * (n // k) + m
elif n % k > int(k / 2):
    result = 2 * m * (n // k) + 2 * m
else:
    result = 2 * m * (n // k)
print(result)
