maximum = 0
result = 0
while True:
    n = int(input())
    if n != 0:
        if n > maximum:
            result = 0
            maximum = n
        if n == maximum:
            result += 1
    else:
        print(result)
        break
