l1 = int(input())
r1 = int(input())
l2 = int(input())
r2 = int(input())
l3 = int(input())
r3 = int(input())

l1_l2_r1 = l1 <= l2 <= r1
l1_r2_r1 = l1 <= r2 <= r1

l1_l3_r1 = l1 <= l3 <= r1
l1_r3_r1 = l1 <= r3 <= r1

l2_l1_r2 = l2 <= l1 <= r2
l2_r1_r2 = l2 <= r1 <= r2

l2_l3_r2 = l2 <= l3 <= r2
l2_r3_r2 = l2 <= r3 <= r2

l3_l1_r3 = l3 <= l1 <= r3
l3_r1_r3 = l3 <= r1 <= r3

l3_l2_r3 = l3 <= l2 <= r3
l3_r2_r3 = l3 <= r2 <= r3

something_1 = (l1_l2_r1 or l1_r2_r1) or (l1_l3_r1 or l1_r3_r1)
something_2 = (l2_l1_r2 or l2_r1_r2) or (l2_l3_r2 or l2_r3_r2)
something_3 = (l3_l1_r3 or l3_r1_r3) or (l3_l2_r3 or l3_r2_r3)

if something_1 and something_2 and something_3:
    print(0)
else:
    if not something_1 and not something_2 and not something_3:
        pass
