result = 0

while True:
    n = int(input())
    if n != 0:
        if n % 2 == 0:
            result += 1
    else:
        break

print(result)
